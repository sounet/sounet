<?php
session_start();
require ("class/config.php");
require ("class/Template.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$TplView = new Template("view/index.html");

if(isset($_GET['Secao'])) {
    $Sec = $_GET['Secao'];
    $url = explode('/', $_GET['Secao']);
}else{
    $Sec = "";
    $url[0] = "";
}

if(($url[0] == "") || ($url[0] == "Inicial")) {
    require("controller/home.php");
} else {
    require("controller/secao.php");
}

$TplView->NOME_PROJETO = NOME_PROJETO;
$TplView->SLOGAN_PROJETO = SLOGAN_PROJETO;

$TplView->URL_BASE = URL_BASE;

$TplView->show();
?>