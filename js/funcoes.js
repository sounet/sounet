$(document).ready(function(){
	//Send forms simples
    $("#form_send, #two_forms_page, #there_forms_page, #contact-form").submit(function () {
        var form = $(this);
        var url_post = $(this).attr('action');

        //Post CKEditor
        if(form.find('#descCK')){
            try{
                for(instance in CKEDITOR.instances){
                    CKEDITOR.instances[instance].updateElement();
                }
            }catch(ex){}
        }

        var data = $(this).serialize();

        $.ajax({
            url: url_post,
            data: data,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                form.find('.return_form').fadeIn('fast').html("<img src='./images/loader.gif' width='25' height='25' />");
            },
            success: function (retorno) {
                if (retorno.error) {
                    form.find('.return_form').html('<div class="error_form">' + retorno.error + '</div>');
                } else if (retorno.success) {
                    form.find('.return_form').html('<div class="success_form">' + retorno.success + '</div>');
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 2000);
                } else if (retorno.success_modal) {
                    form.find('.return_form').html(swal("Sucesso!", retorno.success_modal, "success"));
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 4000); 
                }else{
                    alert("Houve algum problema!");
                }
            }
        });

        return false;
    });

    //Post delete itens
    $(".deleteitem").click(function(){
        var itemdel = $(this);
        $.ajax({
            url: itemdel.attr("href"),
            type: 'POST',
            dataType: 'json',
            data: 'idfinfo='+itemdel.attr("id")+'&tpf='+itemdel.attr("rel"),
            success: function (retorno) {
                if(retorno.error){
                    alert(retorno.error);
                }else if(retorno.success) {

                    itemdel.parent().parent().fadeOut('slow', function(){
                        itemdel.remove();
                    });

                }else{
                    alert("Houve algum problema!");
                }
            }
        });

        return false;
    });

    //Post Form com upload
    $("#form_send_upload").on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        var url_post = $(this).attr('action');
        //Post CKEditor
        if(form.find('#descCK')){
            try{
                for(instance in CKEDITOR.instances){
                    CKEDITOR.instances[instance].updateElement();
                }
            }catch(ex){}
        }

        //var data = $(this).serialize();
        $.ajax({
            url: url_post,
            type: 'POST',
            dataType: 'json',      
            data: new FormData(this),
            contentType: false,       
            cache: false,           
            processData:false,
            beforeSend: function () {
                form.find('.return_form').fadeIn('fast').html("<img src='/images/loader.gif' width='25' height='25' />");
            },
            success: function (retorno) {
                if (retorno.error) {
                    form.find('.return_form').html('<div class="error_form">' + retorno.error + '</div>');
                } else if (retorno.success) {
                    form.find('.return_form').html('<div class="success_form">' + retorno.success + '</div>');
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 2000);
                } else if (retorno.success_modal) {
                    form.find('.return_form').html(swal("Sucesso!", retorno.success_modal, "success"));
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 4000); 
                }else{
                    alert("Houve algum problema!");
                }
            }
        });
    });
});

/*MASCARA DE TELEFONE*/

 $("#fone").bind('input propertychange',function(){
 
    var texto = $(this).val();
    
    texto = texto.replace(/[^\d]/g, '');
    
    if (texto.length > 0)
    {
    texto = "(" + texto;
        
        if (texto.length > 3)
        {
            texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');  
        }
        if (texto.length > 12)
        {      
            if (texto.length > 13) 
                texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
            else
                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
        }                 
            if (texto.length > 15)                
               texto = texto.substr(0,15);
    }
   $(this).val(texto);     
})

/*FIM DA MASCARA*/