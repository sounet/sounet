<?php
session_start();
require ("../class/config.php");
$InforVal = new Valida();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//Verifica se todos os campos estão preenchidos e se o tpf veio preenchido
if (empty($Info_post) || (!isset($Info_post['tpf']) || empty($Info_post['tpf']))) {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

$JsonResults = array();

if($InforVal->CheckAes($Info_post['tpf']) == "sendContato"){

    //Remove o direcionamento do bloco
   unset($Info_post['tpf']);

   $MsgEmail = file_get_contents(__DIR__."/../Emails/contato.html");
   $MsgEmail = str_replace ('%Nome', $Info_post['nome'], $MsgEmail);
   $MsgEmail = str_replace ('%Email', $Info_post['email'], $MsgEmail);
   $MsgEmail = str_replace ('%Telefone', $Info_post['telefone'], $MsgEmail);    
   $result = Valida::EnviarEmail("Contato - Franquia", $MsgEmail, MAILUSER, NOME_REMETENTE, MAIL_REPLY, 'Contato - Franquia');

   if($result){
       $JsonResults['success'] = "OBRIGADO POR DEIXAR SEU CONTATO. EM BREVE UM DE NOSSOS CONSULTORES ENTRARÁ EM CONTATO COM VOCÊ!";
       $JsonResults['urldir'] = URL_DIR;
   }else{
       $JsonResults['error'] = "Falha ao enviar e-mail!";
   }
   
 /*  echo "<script>window.location = '".URL_BASE."ok';</script>";*/

   echo json_encode($JsonResults);
   exit();

}else {
   echo "<script>window.location='".URL_BASE."';</script>";
   exit();
}
?>