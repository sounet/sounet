<?php
session_start();
require ("../class/config.php");
$InforVal = new Valida();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//Verifica se todos os campos estão preenchidos e se o tpf veio preenchido
if (empty($Info_post) || (!isset($Info_post['tpf']) || empty($Info_post['tpf']))) {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

$JsonResults = array();

if($InforVal->CheckAes($Info_post['tpf']) == "sendContato"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
    
    //Envia e-mail contato
    $Contato = new Contato();
    $Contato->ExeCadastro($Info_post);
    if($Contato->GetResult()){
        $JsonResults['success'] = Mensagens::SetEmailSuccess();
        $JsonResults['urldir'] = URL_BASE;
    }else{
        $JsonResults['error'] = $Contato->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "sendAgendamento"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);

    $MsgEmail = file_get_contents(__DIR__."/../Emails/agendamento.html");
    $MsgEmail = str_replace ('%Nome', $Info_post['Name'], $MsgEmail);
    $MsgEmail = str_replace ('%Email', $Info_post['Email'], $MsgEmail);
    $MsgEmail = str_replace ('%Telefone', $Info_post['Telefone'], $MsgEmail);    
    $MsgEmail = str_replace ('%Mensagem', $Info_post['Message'], $MsgEmail);
    $result = Valida::EnviarEmail("Agendamento - Site MeuImplante", $MsgEmail, MAILUSER, NOME_REMETENTE, MAIL_REPLY, 'Contato - MeuImplante');

    if($result){
        $JsonResults['success'] = "Agendamento enviado com sucesso!";
        $JsonResults['urldir'] = URL_BASE;
    }else{
        $JsonResults['error'] = "Falha ao enviar e-mail!";
    }
    
    echo json_encode($JsonResults);
    exit();

}else {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}
?>