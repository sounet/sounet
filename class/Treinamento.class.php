<?php
Class Treinamento{

    private $nome;
    private $video;
    private $descricao;

    public function registrar(){
        $this->alimentar($_POST);
        $banco = new Banco;
        return $banco->create('treinamento',array(
            'nome'=>$this->nome,
            'video'=>$this->video,
            'descricao'=>$this->descricao
        ));
    }

    private function alimentar($treinamento){
        $this->nome = $treinamento['nome'];
        $this->video = $treinamento['video'];
        $this->descricao = $treinamento['descricao'];
    }

    public function listar(){
        $banco = new Banco;
        $return = $banco->read('treinamentos');
        return $return;
    }

}
    
?>