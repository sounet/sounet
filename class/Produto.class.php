<?php

Class Produto{
    private $id;
    private $nome;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("produto");
        return $return;
    }

    public function buscarId($id){
        $banco = new Banco;
        $return = $banco->read("produto",array('id'=>$id));
        return $return;
    }
}

?>