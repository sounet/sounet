<?php

class Cidade{
    private $nome;
    private $estado;


    public function listaCidades(){
        $banco = new Banco;
        $return = $banco->read("cidade");
        foreach($return as $value){
            $cidades[$value->id] = $value->nmmunic;
        }
        return $cidades;
    }

    public function listaEstados(){
        $banco = new Banco;
        $return = $banco->read("cidade");
        foreach($return as $value){
            if(!in_array($value->estado,$estados)){
                $estados[] = $value->estado;
            }
        }
        return $estados;
    }
}

?>