<?php

Class Acesso {
    private $codigo;
    private $nome;
    private $login;
    private $senha;
    private $tipo;
    private $lastLogin;

    public function logar($login,$senha){
        $banco = new Banco;
        $result = $banco->read("acesso",array("login"=>$login,"senha"=>$senha,"ativo"=>'S'));
        if(!is_null($result) && isset($result[0])){
            $this->codigo = $result[0]['id'];
            $this->nome = $result[0]['nome'];
            $this->login = $result[0]['login'];
            $this->tipo = $result[0]['tipo'];
            $this->lastLogin = $result[0]['lastLogin'];
            $banco->update("acesso",array('lastLogin'=>date("Y-m-d H:i:s")),array('id'=>$result[0]['id']));
            return true;
        }else{
            return false;
        }
    }

    public function verifica($login,$tipo){
        $banco = new Banco;
        $result = $banco->read("acesso",array("login"=>$login,"tipo"=>$tipo));
        if(!is_null($result)){
            return true;
        }else{
            return false;
        }
    }

    public function buscar($id){
        $banco = new Banco;
        $return = $banco->read("acesso",array('id'=>$id));
        return $return;
    }

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("acesso");
        return $return;
    }


    public function getNome(){
        return $this->nome;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function getCodigo(){
        return $this->codigo;
    }

    public function getLastLogin(){
        return $this->lastLogin;
    }

    public function registrar($tipo='vend'){
        $this->alimentar($_POST,$tipo);
        $banco = new Banco;
        return $banco->create("acesso",array('nome' => $this->nome, 'login' => $this->login, 'senha' => $this->senha, 'tipo' => $this->tipo));
    } 

    private function alimentar($acesso,$tipo){
        $this->nome = $acesso['nome'];
        $this->login = $acesso['login'];
        $this->senha = $acesso['senha'];
        $this->tipo = $tipo;
    }

    public function getId(){
        $banco = new Banco;
        $banco->read("acesso");

    } 

    private function verificarTipo($tipo){
        switch($tipo){
            case "vend":
                return "Vendedor";
                break;
            case "franq":
                return "Franquia";
                break;
            case "admin":
                return "Administrador";
                break;
            default:
                return "Vendedor";
                break;
        }
    }

    public function alteraAtivo($id){
        $banco = new Banco;
        $acesso = $banco->read("acesso",array('id'=>$id));
        if($acesso[0]['ativo'] == "S"){$altera = "N";}else{$altera = "S";}
        $banco->update("acesso",array('ativo'=>$altera),array('id'=>$id));
    }
    public function atualizar($id, $login, $senha){
        $banco = new Banco;
        $banco->update('acesso',array('login'=>$login,'senha'=>$senha),array('id' => $id));
    }

    public function alterarAtivo(){
        $banco = new Banco;
        $banco->read();
        $banco->update('acesso',array('status'=>'S'));
    }
}
?>