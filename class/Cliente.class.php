<?php

Class Cliente{
    private $cod_cliente;
    private $nome;
    private $cpf_cnpj;
    private $celular;
    private $email;
    private $cep;
    private $endereco;
    private $bairro;
    private $numero;
    private $cidade;
    private $estado;
    private $origem;
    private $origem_cliente;
    private $origem_criacao;
    private $iugu;
    private $ativo;
    private $data;

    public function buscar($cod_cliente){
        $banco = new Banco;
        $return = $banco->read("clientes",array('cod_cliente'=>$cod_cliente));
        return $return;
    }
    public function buscarCpf($cpf){
        $banco = new Banco;
        $return = $banco->read("clientes",array('cpf_cnpj'=>$cpf));
        return $return;
    }
    public function listar(){
        $banco = new Banco;
        $return = $banco->read("clientes");
        return $return;
    }
    public function listarNome($nome){
        $banco = new Banco;
        $return = $banco->read("clientes","WHERE nome LIKE :nome",array('nome'=>$nome."%"));
        return $return;
    }

    public function registrar($post){
        $banco = new Banco;
        $this->alimentar($post);
        $banco->create("clientes",array(
            'cod_cliente' => $this->cod_cliente,
            'nome' => $this->nome,
            'cpf_cnpj' => $this->cpf_cnpj,
            'celular' => $this->celular,
            'email' => $this->email,
            'cep' => $this->cep,
            'endereco' => $this->endereco,
            'bairro' => $this->bairro,
            'numero' => $this->numero,
            'cidade' => $this->cidade,
            'estado' => $this->estado,
            'origem' => $this->origem,
            'origem_cliente' => $this->origem_cliente,
            'origem_criacao' => $this->origem_criacao,
            'iugu' => $this->iugu,
            'ativo' => $this->ativo,
            'data' => $this->data
        ));
    }

    private function alimentar($post){
        if(isset($post['cod_cliente'])){
            $this->cod_cliente = $post['cod_cliente'];
        }else{
            $this->cod_cliente = $this->geraCodigo();
        }
        $this->nome = $post['nome'];
        $this->cpf_cnpj = $post['cpf_cnpj'];
        $this->celular = $post['celular'];
        $this->email = $post['email'];
        $this->cep = $post['cep'];
        $this->endereco = $post['endereco'];
        $this->bairro = $post['bairro'];
        $this->numero = $post['numero'];
        $this->cidade = $post['cidade'];
        $this->estado = $post['estado'];
        $this->origem = 0;
        $this->origem_cliente = 'Painel Franqueado';
        $this->origem_criacao = 'Painel Franqueado';
        $this->iugu = 0;
        $this->ativo = 1;
        $hoje = new DateTime;
        $this->data = $hoje->format("Y-m-d H:i:s");
    }

    private function geraCodigo(){
        return chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90));
    }

}


?>