<?php
class Arquivo{

    public $categoria;
    public $nome;
    public $file;


    public function listar(){
        $banco = new Banco;
        $return = $banco->read("arquivo", NULL, NULL, "ORDER BY nome");
        return $return;
    }
    public function listarPorCategoria($categoria){
        $banco = new Banco;
        $return = $banco->read("arquivo"," WHERE categoria = :categoria AND status = 'S'", array('categoria'=>$categoria), "ORDER BY nome");
        return $return;
    }
    private function alimentar($post){
        $this->categoria = $post['categoria'];
        $this->nome = $post['nome'];
    }
    public function remover($id){
        $banco = new Banco;
        $banco->update("arquivo",array('status'=>'N'),array('id'=>$id));
    }
    public function registrar($post,$file){
        $this->alimentar($post);
        $banco = new Banco;
        $datafile = array(
            'categoria'=>$this->categoria,
            'nome'=>$this->nome,
            'arquivo'=>$file
        );
        $banco->create("arquivo",$datafile);
    }
}
?>