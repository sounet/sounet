<?php
/**
 * <strong>Valida.class</strong>
 * Classe responsável por validar informações do sistema
 * @copyright (c) 2016, André Cristhian
 */
class Valida{
    private static $Data;
    private static $Format;
    
    public static function Email($email){
        self::$Data = (string) $email;
        self::$Format = "/^[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*@[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$/";
        if (preg_match(self::$Format, self::$Data)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function validaCPF($cpf){
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);
        if (strlen($cpf) <> 11) {
            return false;
        }
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} <> $d) {
                return false;
            }
        }
        return true;
    }
/*    
    public static function validaCNPJ($cnpj){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        $invalidos = ('00000000000000', '11111111111111', '22222222222222', '33333333333333', '44444444444444', '55555555555555', '66666666666666', '77777777777777', '88888888888888', '99999999999999');

        if(strlen($cnpj) <> 14){
            return false;
        }elseif (in_array($cnpj, $invalidos)){  
            return false;
        }

        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++){
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj{12} <> ($resto < 2 ? 0 : 11 - $resto)){
            return false;
        }

        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++){
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
*/
    public static function validaData($data){
        $data = explode("/", $data);
        if (checkdate($data[1], $data[0], $data[2])) {
            return true;
        }else{
            return false;
        }
    }

    public static function StrUrl($string){
        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyrr                                 ';  
        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        $string = strip_tags(trim($string));
        $string = str_replace(" ","-",$string);
        $string = str_replace(array("-----","----","---","--"),"-",$string);
        return strtolower(utf8_encode($string));
    }
    
    public static function UrlAtual(){
        return "http://" . $_SERVER['SERVER_NAME'] . $_SERVER ['REQUEST_URI'];
    }
    
    public static function DataForSql($data){
        return implode("-", array_reverse(explode("/", $data)));
    }
    
    public static function DataForUser($data){
        return str_replace("-", "/", implode('-', array_reverse(explode('-', $data))));
    }
    
    public static function GeraAes($pass){
        self::$Data = $pass;
        self::$Data = AesCtr::encrypt(self::$Data, "mi@dia#", 256);
        self::$Data = base64_encode(self::$Data);
        return self::$Data;
    }
    
    public static function CheckAes($pass){
        self::$Data = $pass;
        self::$Data = base64_decode(self::$Data);
        self::$Data =  AesCtr::decrypt(self::$Data, "mi@dia#", 256);
        return self::$Data;
    }
    
    public static function GeraCodigo($lenght = 8){
        $returnCode = '';
        $chars = '';
        $chars .= strtoupper('abcdefghijkmnpqrstuvwxyz');
        $chars .= 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $chars .= '123456789';
        $count = strlen($chars);
        for ($n = 1; $n <= $lenght; $n++) {
            $rand = mt_rand(1, $count);
            $returnCode .= $chars[$rand - 1];
        }
        return $returnCode;
    }
    
    public static function EnviarEmail($assunto,$msg_mail,$emailremetente,$nomeremetente,$emaildestino,$nomedestino){
        require_once(__DIR__."/../../class/Library/PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 1;
        $mail->SMTPAuth = true;
        $mail->IsHTML(true);
        $mail->Host = MAILHOST;
        $mail->Username = MAILUSER;
        $mail->Password = MAILPASS;
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';
        $mail->FromName = $nomeremetente;
        $mail->From = MAILUSER;
        $mail->AddReplyTo($emailremetente, $nomeremetente);
        $mail->Subject = $assunto;
        $mail->MsgHTML($msg_mail);
        $mail->AddAddress($emaildestino,$nomedestino);
        if($mail->Send()){
            return true;
        }else{
            return false;
        }
    }
    
    public static function GeraHash($tempo, $slat, $senha){
        return crypt($senha, '$2a$'.$tempo.'$'.$slat.'$');
    }

    public static function CheckHash($senha_post, $senha_bdcrypt){
        return (crypt($senha_post, $senha_bdcrypt) === $senha_bdcrypt);
    }

    public static function Base3($senha){
        return base64_encode(base64_encode(base64_encode($senha)));
    }

    public static function Rebase3($senha){
        return base64_decode(base64_decode(base64_decode($senha)));
    }
}
