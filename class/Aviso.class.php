<?php

class Aviso{
    private $nome;
    private $mensagem;
    private $data;
    private $franquiaVe;
    private $vendedorVe;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("aviso",null,null,"ORDER BY id Desc");
        return $return;
    }

    public function registrar(){
        $banco = new Banco;
        $this->alimentar($_POST);
        $return = $banco->create("aviso",array('nome'=>$this->nome,'mensagem'=>$this->mensagem,'data'=>$this->data,'franquiaVe'=>$this->franquiaVe,'vendedorVe'=>$this->vendedorVe));
        return $return;
    }

    private function alimentar($post){
        $this->nome = $post['nome'];
        $this->mensagem = $post['mensagem'];
        $data = new DateTime;
        $this->data = $data->format("Y-m-d H:i:s");
        if(isset($post['franquiaVe'])){
            $this->franquiaVe = $post['franquiaVe'];
        }else{
            $this->franquiaVe = 0;
        }
        if(isset($post['vendedorVe'])){
            $this->vendedorVe = $post['vendedorVe'];
        }else{
            $this->vendedorVe = 0;
        }
    }
}
?>