<?php
class Observacoes{
    public $nome;
    public $texto;

    public function registrar($nome,$texto,$cod_assinatura){
        $banco = new Banco;
        $data = new DateTime;
        $dados = array('nome_postador'=>$nome,'observacao'=>$texto,'cod_assinatura'=>$cod_assinatura);
        $return = $banco->create("observacoes",$dados);

        $gatilho = new Gatilho;
        $dados['type_action'] = "observacoes";
        $return = $gatilho->request("POST",$dados);

        return $return;
    }

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("observacoes");
        return $return;
    }

    public function buscar($cod_assinatura){
        $banco = new Banco;
        $return = $banco->read("observacoes",array('cod_assinatura'=>$cod_assinatura));
        return $return;
    }
}
?>