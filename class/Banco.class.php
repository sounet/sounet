<?php
Class Banco {

    //Informacoes para conexao
    private $host = HOST;
    private $user = USER;
    private $pass = PASS;
    private $dbnm = DATA;
    private $dial;
    private $conn;

    //Informacoes para solicitacoes
    private $table;
    private $data;
    private $where = null;
    private $order;
    private $result;

    //metodos aplicados
    private $create;
    private $read;
    private $update;
    private $delete;

    public function __construct(){
    }

    public function getResult(){
        return $this->result;
    }


    private function conect(){
        $this->dial = "mysql:host=".$this->host.";dbname=".$this->dbnm.";charset=utf8";
        try{
            $this->conn = new PDO($this->dial, $this->user, $this->pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        }catch(PDOException $e){
            echo "Falha na conexao:".$e->getMessage();
        }
    }

    private function getSyntax($type){
        if($type == "create"){
            $fields = implode(", ", array_keys($this->data));
            $values = ":". implode(", :", array_keys($this->data));
            $this->create = "INSERT INTO {$this->table} ({$fields}) VALUES ({$values})";
        }elseif($type == "read"){
            $this->read = "SELECT * FROM {$this->table} ";

            if (is_array($this->data)) {
                $this->read .= "WHERE ";
                foreach($this->data as $keys => $value){
                    if(isset($rep)){
                        $this->read .= " AND ";
                    }
                    $this->read .= $keys." = '".$value."'";
                    $rep = 1;
                }
                $this->read .= $this->order;
                unset($rep);
            } else {
                if(!is_null($this->order)){
                    $this->data .= $this->order;
                }
                if (!is_null($this->where)) {
                    $this->read = $this->conn->prepare($this->read . $this->data);
                    foreach ($this->where as $Vinculos => $Valores) {
                        $this->read->bindValue(":{$Vinculos}", $Valores, (is_int($Valores) ? PDO::PARAM_INT : PDO::PARAM_STR));
                    }
                }else{
                    $this->read .= $this->order;
                }
            }
            

        }elseif($type == "update"){
            if ($this->where != null) {

                foreach ($this->data as $keys => $values) {
                    $fields[] = $keys . " = :" . $keys;
                }

                $dataString = implode(", ", $fields);
                $this->update = "UPDATE {$this->table} SET {$dataString} ";
                if(is_array($this->where)){
                    $this->update .= " WHERE ";
                    foreach($this->where as $keys => $value){
                        if(isset($rep)){
                            $this->update .= " AND ";
                        }
                        $this->update .= $keys." = :".$keys;
                        $rep = 1;
                    }
                }else{
                    $this->update .= $this->where;
                }
                unset($rep);

            }
        }elseif($type == "delete"){

        }
    }

    public function create($table,$fieldsArray){
        $this->table = $table;
        $this->data = $fieldsArray;
        return $this->exeCreate();
    }
    
    private function exeCreate(){
        $this->conect();
        try{
            $this->getSyntax("create");
            $this->create = $this->conn->prepare($this->create);
            $this->create->execute($this->data);
            $this->result = $this->conn->lastInsertId();
        }catch(Exception $e){
            $this->result = false;
            echo "<b>Erro ao cadastrar: </b>{$e->getMessage()}. <br><b>Codigo: </b>{$e->getCode()}";
        }
        return $this->result;
    }

    public function read($table,$dataArray=null,$parseString=null,$order=null){
        $this->table = $table;
        $this->data = $dataArray;
        $this->where = $parseString;
        $this->order = $order;

        return $this->exeRead();
    }

    private function exeRead(){
        $this->conect();
        try{
            $this->getSyntax("read");
            if ($this->where){
                $this->read->execute($this->where);
                $this->result = $this->read->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $consulta = $this->conn->query($this->read);
                if($consulta){
                    $this->result = array();
                    while($linha = $consulta->fetch(PDO::FETCH_ASSOC)){
                        $this->result[] = $linha;
                    }
                }
            }
        }catch(PDOException $e){
            $this->result = false;
            echo "<b>Erro ao consultar: </b>{$e->getMessage()}. <br><b>Codigo: </b>{$e->getCode()}";
        }
        return $this->result;
    }

    public function update($table,$dataArray,$condition){
        $this->table = $table;
        $this->data = $dataArray;
        $this->where = $condition;
        $this->getSyntax("update");
        return $this->exeUpdate();
    }

    private function exeUpdate(){
        $this->conect();
        try{
            $this->update = $this->conn->prepare($this->update);
            if(is_array($this->where)){
                $array = array_merge($this->data,$this->where);
                $this->update->execute($array);
            }else{ 
                $this->update->execute();
            }
            $this->result = $this->update->rowCount();
        }catch(PDOException $e){
            $this->result = false;
            echo "<b>Erro ao atualizar: </b>{$e->getMessage()}. <br><b>Codigo: </b>{$e->getCode()}";
        }
        return $this->result;
    }

    public function delete($table,$dataArray){
        $this->table = $table;
        $this->data = $dataArray;
        $this->getSyntax("delete");
        $this->exeDelete();
    }
    
    private function exeDelete(){
        
    }
    
}
?>