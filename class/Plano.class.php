<?php

Class Plano{
    private $nome;
    private $produto;
    private $identificador;
    private $codigo;
    private $valor;
    private $comissao;
    private $renovacao;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("plano");
        return $return;
    }

    public function listarPorProduto(){
        $banco = new Banco;
        $return = $banco->read("plano",NULL,NULL,"ORDER BY produto");
        return $return;
    }

    public function listarPorProdutoAtivo(){
        $banco = new Banco;
        $return = $banco->read("plano",array('ativo'=>'S'),NULL,"ORDER BY produto");
        return $return;
    }

    public function buscar($id){
        $banco = new Banco;
        $return = $banco->read("plano",array('id'=>$id));
        return $return;
    }

    public function buscarIdentificador($identificador){
        $banco = new Banco;
        $return = $banco->read("plano",array('identificador'=>$identificador));
        return $return;
    }

    public function registrar($post){
        $this->alimentar($post);
        $banco = new Banco;
        $banco->create("plano",array(
            'nome'=>$this->nome,
            'produto'=>$this->produto,
            'identificador'=>$this->identificador,
            'codigo'=>$this->codigo,
            'valor'=>$this->valor,
            'comissao'=>$this->comissao,
            'renovacao'=>$this->renovacao
        ));
    }
    private function alimentar($post){
        $this->nome = $post['nome'];
        $this->produto = $post['produto'];
        $this->identificador = $post['identificador'];
        $this->codigo = $post['codigo'];
        $this->valor = $post['valor'];
        $this->comissao = $post['comissao'];
        $this->renovacao = $post['renovacao'];
    }

    public function atualizar($post){
        $this->alimentar($post);
        $banco = new Banco;
        $banco->update("plano",array(
            'nome'=>$this->nome,
            'produto'=>$this->produto,
            'identificador'=>$this->identificador,
            'codigo'=>$this->codigo,
            'valor'=>$this->valor,
            'comissao'=>$this->comissao,
            'renovacao'=>$this->renovacao
        ),array('id'=>$post['id']));
    }
}

?>