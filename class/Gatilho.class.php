<?php
class Gatilho{
    private $error;
    private $result;
    private $endpoint;

    public function __construct() {
        $this->endpoint = 'https://www.midiagram.com.br/Requests/api_request.php';
        //$this->endpoint = AMAZON_SERVER . ENDPOINT_UPDATE_PAINEL . '.php';
    }
        
    private function _defaultHeaders($headers = null) {
        $headers[] = 'Accept: application/json';
        $headers[] = 'Accept-Charset: utf-8';
        $headers[] = 'User-Agent: SouNet PHPLibrary';
        $headers[] = 'Accept-Language: pt-br;q=0.9,pt-BR';
    
        return $headers;
    }

    public function setEndpoint($url = null) {
        if ($url) {
            $this->endpoint = $url . '.php';
        }
    }
    
    public function getEndpoint() {
        return $this->endpoint;
    }
    
    public function getResult() {
        return $this->result;
    }

    public function getError() {
        return $this->error;
    }

    public function request($method = 'POST', $data, $json = true) {
        $headers = $this->_defaultHeaders();

        list($response_body, $response_code) = $this->requestWithCURL($method, $headers, $data);

        $response = json_decode($response_body);

        /*if (json_last_error() != JSON_ERROR_NONE) {
            echo 'erro json';
        }
        
        if ($response_code == 404) {
            echo 'erro 404';
        }*/

        if (isset($response->errors)) {
            if ((gettype($response->errors) != 'string') && count(get_object_vars($response->errors)) == 0) {
                unset($response->errors);
            } elseif ((gettype($response->errors) != 'string') && count(get_object_vars($response->errors)) > 0) {
                $response->errors = (array) $response->errors;
            }

            if (isset($response->errors) && (gettype($response->errors) == 'string')) {
                $response->errors = $response->errors;
            }
        }

        return $response;
    }

    private static function arrayToParams($array, $prefix = null) {
        if (!is_array($array)) {
            return $array;
        }

        $params = null;

        foreach ($array as $k => $v) {
            if (is_null($v)) {
                continue;
            }

            if ($prefix && $k && !is_int($k)) {
                $k = $prefix.'['.$k.']';
            } elseif ($prefix) {
                $k = $prefix.'[]';
            }

            if (is_array($v)) {
                $params[] = self::arrayToParams($v, $k);
            } else {
                $params[] = $k.'='.urlencode($v);
            }
        }

        return implode('&', $params);
    }

    private function requestWithCURL($method, $headers, $data) {
        $curl = curl_init();

        $opts = null;

        $data = self::arrayToParams($data);

        if (strtolower($method) == 'post') {
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $data;
        }
        if (strtolower($method) == 'delete') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        }

        if (strtolower($method) == 'put') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'PUT';
            $opts[CURLOPT_POSTFIELDS] = $data;
        }

        $opts[CURLOPT_URL] = $this->endpoint;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_CONNECTTIMEOUT] = 30;
        $opts[CURLOPT_TIMEOUT] = 80;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_HTTPHEADER] = $headers;

        curl_setopt_array($curl, $opts);

        $response_body = curl_exec($curl);
        $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $this->result = array($response_body, $response_code);
    }

}
?>