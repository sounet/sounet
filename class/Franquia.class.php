<?php

Class Franquia{
    private $nome;
    private $razao;
    private $endereco;
    private $estado;
    private $cidade;
    private $responsavel;
    private $contato;
    private $celular;
    private $qtAcesso;
    private $codAcesso;
    private $dataReg;
    private $pendencia;

    private $result;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("franquia");
        return $return;
    }

    public function buscar($id){
        $banco = new Banco;
        $return = $banco->read("franquia",array('id'=>$id));
        return $return;
    }

    public function buscarFranquia($codAcesso){
        $banco = new Banco;
        $return = $banco->read("franquia",array('codAcesso'=>$codAcesso));
        return $return;
    }

    public function buscarVendedores($id){
        $banco = new Banco;
        $return = $banco->read("vendedor",array('codFranquia'=>$id));
        return $return;
    }

    public function getResult(){
        return $this->result;
    }

    public function registrar($codAcesso){
        $this->alimentar($_POST,$codAcesso);
        $banco = new Banco;
        $hoje = new DateTime();
        return $banco->create("franquia",array(
            'nome'=>$this->nome,
            'razao'=>$this->razao,
            'endereco'=>$this->endereco,
            'responsavel'=>$this->responsavel,
            'contato'=>$this->contato,
            'celular'=>$this->celular,
            'codAcesso'=>$this->codAcesso,
            'qtAcesso'=>$this->qtAcesso,
            'dataReg'=>$hoje->format("Y-m-d H:i:s")
        ));
    }

    private function alimentar($franquia,$codAcesso){
        $this->nome = $franquia['nome'];
        $this->razao = $franquia['razao'];
        $this->endereco = $franquia['endereco'];
        $this->estado = $franquia['estado'];
        $this->cidade = $franquia['cidade'];
        $this->responsavel = $franquia['responsavel'];
        $this->contato = $franquia['contato'];
        $this->celular = $franquia['celular'];
        $this->codAcesso = $codAcesso;
        $this->qtAcesso = $franquia['qtAcesso'];
    }

    public function alteraPendencia($id){
        $banco = new Banco;
        $franquia = $banco->read("franquia",array('id'=>$id));
        $pendencia = ($franquia[0]['pendencia'] == 'S')? 'N' : 'S';
        $this->result = $banco->update("franquia",array('pendencia'=>$pendencia),array('id'=>$id));
        return $this->result;
    }
}

?>