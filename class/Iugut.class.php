<?php
Class Iugut{
    private $id = IUGU_SOUNET_ID;
    private $token = IUGU_SOUNET;
    private $url = 'https://api.iugu.com/v1/';
  
    private $Error;
    private $Result;
    private $Endpoint = "https://api.iugu.com/";

    
    private function _defaultHeaders() {
        $headers[] = 'Accept: application/json';
        $headers[] = 'Accept-Charset: utf-8';
        $headers[] = 'User-Agent: SouNet PHPLibrary';
        $headers[] = 'Accept-Language: pt-br;q=0.9,pt-BR';
        $headers[] = 'Authorization: Basic';
    
        return $headers;
    }

    public function GetResult() {
        return $this->Result;
    }
    
    public function GetError() {
       return $this->Error;
    }
    
    public function setEndpoint($url = null) {
        $this->Endpoint = $url;
    }
    
    public function getEndpoint() {
        return $this->Endpoint;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function setToken($token){
        $this->token = $token;
    }

    private static function arrayToParams($array, $prefix = null) {
        if (!is_array($array)) {
            return $array;
        }
        $params;
        foreach ($array as $k => $v) {
            if (is_null($v)) {
                continue;
            }
            if ($prefix && $k && !is_int($k)) {
                $k = $prefix.'['.$k.']';
            } elseif ($prefix) {
                $k = $prefix.'[]';
            }
            if (is_array($v)) {
                $params[] = self::arrayToParams($v, $k);
            } else {
                $params[] = $k.'='.urlencode($v);
            }
        }
        return implode('&', $params);
    }

    public function Request($method = 'POST', $data) {
        $headers = $this->_defaultHeaders();
        list($response_body, $response_code) = $this->requestWithCURL($method, $headers, $data);
        $response = json_decode($response_body);
        if (isset($response->errors)) {
            if ((gettype($response->errors) != 'string') && count(get_object_vars($response->errors)) == 0) {
                unset($response->errors);
            } elseif ((gettype($response->errors) != 'string') && count(get_object_vars($response->errors)) > 0) {
                $response->errors = (array) $response->errors;
            }
            if (isset($response->errors) && (gettype($response->errors) == 'string')) {
                $response->errors = $response->errors;
            }
        }
        return $response;
    }
    private function requestWithCURL($method, $headers, $data) {
        $curl = curl_init();
    
        $opts;
    
        $data = $this->arrayToParams($data);
    
        if (strtolower($method) == 'post') {
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $data;
        }
        if (strtolower($method) == 'delete') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        }
    
        if (strtolower($method) == 'put') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'PUT';
            $opts[CURLOPT_POSTFIELDS] = $data;
        }
    
        if (strtolower($method) == 'get') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'GET';
            $opts[CURLOPT_POSTFIELDS] = $data;
        }

        $opts[CURLOPT_URL] = $this->Endpoint;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_CONNECTTIMEOUT] = 30;
        $opts[CURLOPT_TIMEOUT] = 80;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_HTTPHEADER] = $headers;
    
        curl_setopt_array($curl, $opts);
    
        $response_body = curl_exec($curl);
        $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
        curl_close($curl);
    
        return $this->Result = array($response_body, $response_code);
    }

    public function criaFatura($email,$due_date,$cod_cliente,$items){
        $post = array(
          'email'=>$email,
          'due_date'=>$due_date,
          'items'=>$items,
          'customer_id'=>$cod_cliente
        );
        $this->setEndpoint($this->url.'invoices/'.'?api_token='.$this->token);
        return $this->Request('POST',$post);
    }

    public function criaFaturaCliente($email,$due_date,$cod_cliente,$items,$cep,$number,$street,$district){
        $post = array(
            'email'=>$email,
            'due_date'=>$due_date,
            'items'=>$items,
            'payer'=>array(
                'address'=>array(
                'zip_code'=>$cep,
                'street'=>$street,
                'number'=>$number,
                'district'=>$district
              )
            ),
            'customer_id'=>$cod_cliente

        );
        $this->setEndpoint($this->url.'invoices/'.'?api_token='.$this->token);
        return $this->Request('POST',$post);
    }
    
    public function buscaFatura($cod_fatura){
        $this->setEndpoint($this->url.'invoices/'.$cod_fatura.'?api_token='.$this->token);
        return $this->Request('GET',array('id'=>$cod_fatura));
    }
    
    public function cancelaFatura($cod_fatura){
        $this->setEndpoint($this->url.'invoices/'.'?api_token='.$this->token);
        return $this->Request('PUT',$post);
    }
    
    public function criaCliente($nome,$email,$cpf,$zip_code,$number,$street,$district){
        $this->setEndpoint($this->url.'customers'.'?api_token='.$this->token);
        $post = array(
            'email'=>$email,
            'name'=>$nome,
            'cpf_cnpj'=>$cpf,
            'zip_code'=>$zip_code,
            'street'=>$street,
            'district'=>$district,
            'number'=>$number
        );
        return $this->Request('POST',$post);
    }

    public function buscaCliente($cod_cliente){
        $this->setEndpoint($this->url."customers/".$cod_cliente.'?api_token='.$this->token);
        return $this->Request('PUT',array('id'=>$cod_cliente));
    }

}
?>