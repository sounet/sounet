<?php

Class Vendedor{
    private $codFranquia;
    private $nome;
    private $cpf;
    private $email;
    private $codAcesso;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("vendedor");
        return $return;
    }

    public function listarFranquia($codFranquia){
        $banco = new Banco;
        $return = $banco->read("vendedor",array('codFranquia'=>$codFranquia));
        return $return;
    }

    public function buscar($id){
        $banco = new Banco;
        $return = $banco->read("vendedor",array('id'=>$id));
        return $return;
    }

    public function buscarVendedor($acesso){
        $banco = new Banco;
        $return = $banco->read("vendedor",array('codAcesso'=>$acesso));
        return $return;
    }

    public function registrar($codAcesso){
        $this->alimentar($_POST,$codAcesso);
        $banco = new Banco;
        return $banco->create("vendedor",array(
            'codFranquia'=>$this->codFranquia,
            'nome'=>$this->nome,
            'cpf'=>$this->cpf,
            'email'=>$this->email,
            'codAcesso'=>$this->codAcesso
        ));
    }

    private function alimentar($vendedor,$codAcesso){
        $this->codFranquia = $vendedor['codFranquia'];
        $this->nome = $vendedor['nome'];
        $this->cpf = $vendedor['cpf'];
        $this->email = $vendedor['email'];
        $this->codAcesso = $codAcesso;
        } 
}
?>