<?php

Class Compra{
    private $id_compra;
    private $cod_assinatura;
    private $cod_cliente;
    private $cod_fatura;
    private $plano;
    private $metodo;
    private $origem_compra;
    private $valor;
    private $renovacao;
    private $status_pgto;
    private $status_ass;
    private $revendedor;
    private $cod_vendedor;
    private $dias_adicionados;
    private $comprovante;
    private $ativo;
    private $valor_comissao;
    private $data;

    public function listar(){
        $banco = new Banco;
        $return = $banco->read("compras", NULL, NULL," ORDER BY id DESC");
        return $return;
    }

    public function listarFiltro($filtro,$filtroDados=NULL){
        $banco = new Banco;
        $return = $banco->read("compras",$filtro,$filtroDados," ORDER BY id DESC");
        return $return;
    }

    public function listarFranquia($cod_franquia){
        $banco = new Banco;
        $franquia = new Franquia;
        $return = array();
        foreach($franquia->buscarVendedores($cod_franquia) as $vend){
            $return = array_merge($return,$this->listarVendedor($vend['id']));
        }
        foreach($return as $ret){
            $datas[] = $ret['data'];
        }
        if(isset($datas)){
            array_multisort($datas,SORT_ASC, $return);
        }
        return $return;
    }

    public function listarVendedor($cod_vendedor){
        $banco = new Banco;
        $return = $banco->read("compras",array('cod_vendedor'=>$cod_vendedor));
        return $return;
    }

    public function buscar($id){
        $banco = new Banco;
        $return = $banco->read("compras",array('id'=>$id));
        return $return;
    }

    public function buscarId($id_compra){
        $banco = new Banco;
        $return = $banco->read("compras",array('id_compra'=>$id_compra));
        return $return;
    }

    public function contarProduto($produto){
        $banco = new Banco;
        $return = $banco->read("compras",array('origem_compra'=>$produto));
        return count($return);
    }

    public function convert($value='',$type=''){
        switch($type){
            case "origem":
                return $this->convertOrigem($value);
            break;
            
            case "metodo":
                return $this->convertMetodo($value);
            break;
            
            case "valor":
                return $this->convertValor($value);
            break;
            
            case "status_pgto":
                return $this->convertStatusPgto($value);
            break;
            
            case "data":
                return $this->convertData($value);
            break;
            
            default:
                return $value;
            break;
        }
    }
    private function convertOrigem($metodo){
        $array = array('midiagram'=>'MidiaGram','otimizaragora'=>'Otimizar Agora','sitexis'=>"SiteXis");
        if(array_key_exists($metodo,$array)){
            return $array[$metodo];
        }else{
            return "Outro";
        }
    }
    private function convertMetodo($metodo){
        $array = array('bank_slip'=>'Boleto','credit_card'=>"Cartão de Credito",'deposit'=>"Deposito",'other_method'=>"Outro");
        return $array[$metodo];
    }
    private function convertValor($valor){
        return str_replace(".",",",$valor);
    }

    private function convertStatusPgto($pgto){
        $array = array(
            'pending'           =>"Pendente",
            'paid'              =>"Pago",
            'canceled'          =>"Cancelada",
            'draft'             =>"Rascunho",
            'partially_paid'    =>"Parcialmente paga",
            'refunded'          =>"Reembolsada",
            'expired'           =>"Expirada",
            'in_protest'        =>"Em protesto",
            'chargeback'        =>"Contestada",
            'in_analysis'       =>"Em análise"
        );
        if(array_key_exists($pgto,$array)){
            return $array[$pgto];
        }else{
            return "Outro";
        }
    }

    private function convertData($data){
        $date = new DateTime($data);
        $date->setTimezone(new DateTimeZone("UTC"));
        return $date->format("H:i:s - d/m/Y");
    }

    public function aplicarVendedor($compra,$vendedor){
        $banco = new Banco;
        $return = $banco->update('compras',array('cod_vendedor'=>$vendedor),array('id'=>$compra));
        return $return;
    }

    public function aplicarRenovacao($compra,$renovacao){
        $banco = new Banco;
        $return = $banco->update('compras',array('renovacao'=>$renovacao),array('id'=>$compra));
        return $return;
    }

    public function expirarCompra($compra){
        $banco = new Banco;
        $return = $banco->update('compras',array('status_pgto'=>'expired'),array('id'=>$compra));
        return $return;
    }

    public function registrar($post){
        $this->alimentar($post);
        $banco = new Banco;
        return $banco->create("compras",array(
            'id_compra'=>$this->id_compra,
            'cod_assinatura'=>$this->cod_assinatura,
            'cod_cliente'=>$this->cod_cliente,
            'cod_fatura'=>$this->cod_fatura,
            'plano'=>$this->plano,
            'metodo'=>$this->metodo,
            'origem_compra'=>$this->origem_compra,
            'valor'=>$this->valor,
            'renovacao'=>$this->renovacao,
            'status_pgto'=>$this->status_pgto,
            'status_ass'=>$this->status_ass,
            'revendedor'=>$this->revendedor,
            'cod_vendedor'=>$this->cod_vendedor,
            'dias_adicionados'=>$this->dias_adicionados,
            'comprovante'=>$this->comprovante,
            'ativo'=>$this->ativo,
            'valor_comissao'=>$this->valor_comissao,
            'data'=>$this->data
        ));
    }

    private function alimentar($compra){
        $this->id_compra = rand(9999,99999);
        $this->cod_assinatura = $compra['cod_assinatura'];
        $this->cod_cliente = $compra['cod_cliente'];
        $this->cod_fatura = $compra['cod_fatura'];
        $this->plano = $compra['plano'];
        $this->metodo = $compra['metodo'];
        $this->origem_compra = $compra['origem_compra'];
        $this->valor = $compra['valor'];
        $this->renovacao = $compra['renovacao'];
        $this->status_pgto = $compra['status_pgto'];
        $this->status_ass = $compra['status_ass'];
        $this->revendedor = $compra['revendedor'];
        $this->cod_vendedor = $compra['cod_vendedor'];
        $this->dias_adicionados = $compra['dias_adicionados'];
        $this->comprovante = $compra['comprovante'];
        $this->ativo = $compra['ativo'];
        $this->valor_comissao = $compra['valor_comissao'];
        $this->data = date("Y-m-d H:i:s");
    }
}

?>