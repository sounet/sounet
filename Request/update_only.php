<?php

header("Content-type: text/html; charset=utf-8");
require("../class/config.php");

if(isset($_POST) && !empty($_POST)){

    //Limpa os posts recebidos
    //$CodAssinatura = strip_tags(trim(addslashes($_POST['data']['subscription_id'])));
    $CodId = strip_tags(trim(addslashes($_POST['data']['id'])));
    if(isset($_POST['data']['status'])){$Status = strip_tags(trim(addslashes($_POST['data']['status'])));}

    $banco = new Banco;

    //Mudança de status da fatura
    if ($_POST['event'] == "invoice.status_changed" || $_POST['event'] == "invoice.refund") {

        $result = $banco->read('compras', array( 'cod_fatura' => $CodId ));      
        if ($result) {
            //Atualiza o status
            $update = $banco->update('compras', array('status_pgto' => $Status), array('cod_fatura' => $CodId));

        }

    }

    //Assinatura renovada - Passo 2 - Alterar dizendo que é uma renovação
    // if($_POST['event'] == "subscription.renewed" && $CodId <> ""){

    //     $Read->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1", "cod_assinatura={$CodId}");
    //     if($Read->GetResult()){

    //         //Atualiza a compra
    //         $UpdateStatus = new Update();
    //         $UpdateStatus->ExeUpdate("compras", array("renovacao" => "2"), "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$Read->GetResult()[0]['cod_assinatura']}");
            
    //     }

    // }

    //Assinatura suspensa
    if($_POST['event'] == "subscription.suspended" && $CodId <> ""){

        $compra = $banco->read('compras', 'WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1', array('cod_assinatura' => $CodId));

        if($compra){

            //Atualiza o status
            $update = $banco->update('compras', array("status_ass" => "1"), array('cod_assinatura' => $compra[0]['cod_assinatura']));
            
        }

    }

    //Assinatura ativada
    if($_POST['event'] == "subscription.activated" && $CodId <> ""){

        $compra = $banco->read('compras', 'WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1', array('cod_assinatura' => $CodId));

        if($compra){

            //Atualiza a compra
            $update = $banco->update('compras', array('status_ass' => '0'), array('cod_assinatura' => $compra[0]['cod_assinatura']));
            
        }

    }

}
?>