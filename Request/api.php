<?php

header('Content-type: text/html; charset=utf-8');
require('../class/config.php');

function array_map_recursive($callback, $array) {
    foreach ($array as $key => $value) {
        if (is_array($array[$key])) {
            $array[$key] = array_map_recursive($callback, $array[$key]);
        }
        else {
            $array[$key] = call_user_func($callback, $array[$key]);
        }
    }
    return $array;
}

if (isset($_POST) && !empty($_POST)) {

    //Limpa e retira quaisquer meios de invasões
    $dados = array_map_recursive('strip_tags', $_POST);
    $dados = array_map_recursive('trim', $_POST);

    //Verifica se todos os campos estão preenchidos e se o type_action veio preenchido
	if (empty($dados) || (!isset($dados['type_action']) || empty($dados['type_action']))) {
		$data = array();
	} else {
		$acao = $dados['type_action'];
		unset($dados['type_action']);

		$success = false;
		$banco = new Banco;

		if ($acao == 'clientes') {
			if (isset($dados['where']) && !empty($dados['where'])) {
				$result = $banco->read('clientes', $dados['where']);
			} else {
				$result = $banco->read('clientes', array('cod_cliente' => $dados['cod_cliente']));
			}
			$data = ($result) ? $result[0] : $result ;

		} elseif ($acao == 'plano') {
			$result = $banco->read('plano', array('identificador' => $dados['identificador']));
			$data = ($result) ? $result[0] : $result ;

		} elseif ($acao == 'produtos') {
			$result = $banco->read('plano', array('identificador' => $dados['identificador']));
			$data = ($result) ? $result[0] : $result ;

		} elseif ($acao == 'compras') {
			$result = $banco->read('compras', array('id_compra' => $dados['id_compra'], 'origem_compra' => $dados['origem_compra']));
			$data = ($result) ? $result[0] : $result ;

		} elseif ($acao == 'vendedor') {
			$result = $banco->read('vendedor', array('id' => $dados['cod_vendedor']));
			$data = ($result) ? $result[0] : $result ;
			
		} else {
			$data = array();
		}
	}

	echo json_encode(array('data' => $data));
	exit();

}



?>