<?php

header("Content-type: text/html; charset=utf-8");
require("../class/config.php");

if(isset($_POST) && !empty($_POST)){

    unset($CodAssinatura);
    unset($CodId);
    unset($Status);

    //Limpa os posts recebidos
    if(isset($_POST['data']['event'])){$event = strip_tags(trim(addslashes($_POST['data']['event'])));}else{$event = '';}
    if(isset($_POST['data']['subscription_id'])){$CodAssinatura = strip_tags(trim(addslashes($_POST['data']['subscription_id'])));}else{$CodAssinatura = '';}
    if(isset($_POST['data']['id'])){$CodId = strip_tags(trim(addslashes($_POST['data']['id'])));}else{$CodId = '';}
    if(isset($_POST['data']['status'])){$Status = strip_tags(trim(addslashes($_POST['data']['status'])));}else{$Status = '';}
    if(isset($_POST['data']['account_id'])){$CodConta = strip_tags(trim(addslashes($_POST['data']['account_id'])));}else{$CodConta = '';}

    $banco = new Banco;

    //registra ogatilho do iugu
    $registraIugu = new Banco;
    $registraIugu->create('iugu_gatilho',
        array(
        'evento' => $event,
        'id_fatura' => $CodId,
        'id_conta' => $CodConta,
        'status' => $Status,
        'id_assinatura' => $CodAssinatura,
        'data' => date("Y-m-d H:i:s")
        )
    );

    //Mudança de status da fatura
    if ($_POST['event'] == "invoice.status_changed" || $_POST['event'] == "invoice.refund" || $_POST['event'] == "invoice.payment_failed") {

        $result = $banco->read('compras', array( 'cod_fatura' => $CodId ));      
        if ($result) {
            //Atualiza o status
            $update = $banco->update('compras', array('status_pgto' => $Status), array('cod_fatura' => $CodId));

        }
    }

    //Assinatura renovada - Passo 1 - Criar fatura
    if ($_POST['event'] == "invoice.created" && $CodAssinatura <> "") {

        //Busca compra
        $compra = $banco->read('compras', 'WHERE cod_assinatura = :cod_assinatura AND cod_fatura <> :cod_fatura ORDER BY id DESC', array('cod_assinatura' => $CodAssinatura, 'cod_fatura' => $CodId));

        if ($compra && $compra[0]['status_pgto'] == 'paid') { //Renovação
            //Veirifica taxa de comissao
            $planos = new Plano;
            $plano = $planos->buscarIdentificador($compra[0]['plano']);
            $comissao = $plano[0]['renovacao'];

            //Insere nova compra
            $array_data = array(
                'cod_assinatura'    => $CodAssinatura, 
                'cod_cliente'       => $compra[0]['cod_cliente'], 
                'cod_fatura'        => $CodId, 
                'plano'             => $compra[0]['plano'], 
                'metodo'            => $compra[0]['metodo'], 
                'valor'             => $compra[0]['valor'], 
                'renovacao'         => '2', 
                'status_pgto'       => $Status, 
                'status_ass'        => $compra[0]['status_ass'], 
                'revendedor'        => $compra[0]['revendedor'], 
                'dias_adicionados'  => 1,
                'valor_comissao'    => ($compra[0]['valor']/100*$comissao), 
                'data'              => date('Y-m-d H:i:s')
            );

            $banco->create('compras', $array_data);

        } elseif ($compra && ($compra[0]['status_pgto'] == 'canceled' || $compra[0]['status_pgto'] == 'expired')) { //Segunda via

            //Veirifica taxa de comissao
            $planos = new Plano;
            $plano = $planos->buscarIdentificador($compra[0]['plano']);
            $comissao = $plano[0]['comissao'];
            
            //Insere nova compra
            $array_data = array(
                'cod_assinatura'        => $CodAssinatura, 
                'cod_cliente'           => $compra[0]['cod_cliente'], 
                'cod_fatura'            => $CodId, 
                'plano'                 => $compra[0]['plano'], 
                'metodo'                => $compra[0]['metodo'], 
                'valor'                 => $compra[0]['valor'], 
                'renovacao'             => '1', 
                'status_pgto'           => $Status, 
                'status_ass'            => $compra[0]['status_ass'], 
                'revendedor'            => $compra[0]['revendedor'], 
                'dias_adicionados'      => 2, 
                'valor_comissao'        => ($compra[0]['valor']/100*$comissao), 
                'data' => date('Y-m-d H:i:s')
            );

            $banco->create('compras', $array_data);

        }

    }

    //Assinatura renovada - Passo 2 - Alterar dizendo que é uma renovação
    // if($_POST['event'] == "subscription.renewed" && $CodId <> ""){

    //     $Read->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1", "cod_assinatura={$CodId}");
    //     if($Read->GetResult()){

    //         //Atualiza a compra
    //         $UpdateStatus = new Update();
    //         $UpdateStatus->ExeUpdate("compras", array("renovacao" => "2"), "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$Read->GetResult()[0]['cod_assinatura']}");
            
    //     }

    // }

    //Assinatura suspensa
    if($_POST['event'] == "subscription.suspended" && $CodId <> ""){

        $compra = $banco->read('compras', 'WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1', array('cod_assinatura' => $CodId));

        if($compra){

            //Atualiza o status
            $update = $banco->update('compras', array("status_ass" => "1"), array('cod_assinatura' => $compra[0]['cod_assinatura']));
            
        }

    }

    //Assinatura ativada
    if($_POST['event'] == "subscription.activated" && $CodId <> ""){

        $compra = $banco->read('compras', 'WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1', array('cod_assinatura' => $CodId));

        if($compra){

            //Atualiza a compra
            $update = $banco->update('compras', array('status_ass' => '0'), array('cod_assinatura' => $compra[0]['cod_assinatura']));
            
        }

    }

}
?>