<?php

header('Content-type: text/html; charset=utf-8');
require('../class/config.php');

function respostaGatilho($success = false, $message = 'Ação indisponível!', $dados = array()) {
	echo json_encode(
    	array(
    		'status' => $success,
    		'mensagem' => $message,
    		'dados' => $dados
    	)
    );
    exit();
}


if (isset($_POST) && !empty($_POST)) {

	//Limpa e retira quaisquer meios de invasões
    $dados = array_map('strip_tags', $_POST);
    $dados = array_map('trim', $_POST);

    $status = false;
    $mensagem = 'Ação indisponível';
    $data = array();

    //Verifica se todos os campos estão preenchidos e se o type_action veio preenchido
	if ( empty($dados) || ( !isset($dados['type_action']) || empty($dados['type_action']) ) ) {

		$mensagem = 'Dados Incorretos!';

	} else {

		$acao = $dados['type_action'];
		unset($dados['type_action']);

		if ($acao == 'compras') {

			$banco = new Banco;
	        $result = $banco->read( 'compras', array( 
	        	'id_compra' 	=> $dados['id_compra'],
	        	'origem_compra' => $dados['origem_compra']
	        ));
	        
	        if ($result) {            
	            
	            //UPDATE
	            $status = true;
	            $mensagem = 'Compra existe!';
	            $data = $result;

	        } else{
				//Verifica taxa de comissao
				$planos = new Plano;
				$plano = $planos->buscarIdentificador($dados['plano']);
				if($dados['renovacao'] == 1){
					$comissao = $plano[0]['comissao'];
				}else{
					$comissao = $plano[0]['renovacao'];
				}
				$dados['valor_comissao'] = ($dados['valor']/100*$comissao);


	        	$result = $banco->create( 'compras', $dados);
	        	
	        	if ($result) {        		
	        		$status = true;
        			$mensagem = 'Dados da compra inseridos com sucesso!';
        			$_data = $banco->read( 'compras', array( 'id' => $result ) );
        			$data = $_data[0];

	        	} else {
	        		$mensagem = 'Erro ao inserir dados da compra!';

	        	}

	        }

		} elseif ($acao == 'clientes') {

			$banco = new Banco;
	        $result = $banco->read( 'clientes', array(
	        	'iugu' 				=> $dados['iugu'], 
	        	'cpf_cnpj' 			=> $dados['cpf_cnpj'], 
	        	'email' 			=> $dados['email'],
	        	'origem_cliente'	=> $dados['origem_cliente']
	        ));
	        
	        if ($result) { 
	            
	            //UPDATE
	            $status = true;
	            $mensagem = 'Cliente existe!';
	            $data = $result;

	        } else{

	        	$result = $banco->create('clientes', $dados);
	        	
	        	if ($result) {        		
	        		$status = true;
        			$mensagem = 'Dados inseridos com sucesso!';
        			$_data = $banco->read( 'clientes', array( 'id' => $result ) );
        			$data = $_data[0];

	        	} else {
	        		$mensagem = 'Erro ao inserir dados do cliente!';

	        	}

			}
		} elseif ($acao == 'observacoes') {

			$banco = new Banco;
        	$result = $banco->create('observacoes', $dados);
        	
        	if ($result) {        		
        		$status = true;
      			$mensagem = 'Dados inseridos com sucesso!';
       			$_data = $banco->read( 'observacoes', array( 'id' => $result ) );
       			$data = $_data[0];
        	} else {
        		$mensagem = 'Erro ao inserir dados de observação da compra!';
        	}

		} else {
			$mensagem = 'Ação inexistente!';
		}

	}

	respostaGatilho($status, $mensagem, $data);
}


?>